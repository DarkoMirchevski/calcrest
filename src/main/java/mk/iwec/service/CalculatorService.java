package mk.iwec.service;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class CalculatorService {

	public double getResult(Integer first, Integer second, String function) {
		if (function.equals("+")) {
			return Math.addExact(first, second);
		}
		if (function.equals("-")) {
			return Math.subtractExact(first,second);
		}
		if (function.equals("/")) {
			
		return	Math.floorDiv(first, second);
		}
		if (function.equals("*")) {
			Math.multiplyExact(first, second);
		}
		return 0;
	}
}
