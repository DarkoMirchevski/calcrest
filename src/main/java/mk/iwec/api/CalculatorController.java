package mk.iwec.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import mk.iwec.service.CalculatorService;


@AllArgsConstructor
@RestController
@RequestMapping(path = "/calc")
public class CalculatorController {
	
	@Autowired
	private final CalculatorService cs;
	
	@GetMapping 
	public double getResult(@RequestParam(required = true) Integer firstNumb,
			@RequestParam(required = true) Integer secondNumb, @RequestParam(required = true) String function) {
		return cs.getResult(firstNumb, secondNumb, function);
	}
}
